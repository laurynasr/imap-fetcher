package com.luceneuab;

import de.saly.elasticsearch.maildestination.ElasticsearchMailDestination;
import de.saly.elasticsearch.maildestination.MailDestination;
import de.saly.elasticsearch.support.IndexableMailMessage;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.elasticsearch.common.collect.ImmutableList;
import org.elasticsearch.common.collect.ImmutableSet;
import org.elasticsearch.common.logging.ESLogger;
import org.elasticsearch.common.logging.ESLoggerFactory;

import javax.mail.Message;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;
import java.util.Set;

class LoggableMailDestination implements MailDestination {
    protected final ESLogger logger = ESLoggerFactory.getLogger(this.getClass().getName());
    private final ObjectMapper mapper = new ObjectMapper();

    public LoggableMailDestination() {
        mapper.enable(SerializationConfig.Feature.AUTO_DETECT_FIELDS);//INDENT_OUTPUT);
        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Override
    public void clearDataForFolder(String folderName) throws IOException, MessagingException {

    }

    @Override
    public void close() {

    }

    @Override
    public Set getCurrentlyStoredMessageUids(String folderName, boolean isPop) throws IOException, MessagingException {
        return ImmutableSet.of();
    }

    @Override
    public int getFlaghashcode(String id) throws IOException, MessagingException {
        return 0;
    }

    @Override
    public Set<String> getFolderNames() throws IOException, MessagingException {
        return ImmutableSet.of();
    }

    @Override
    public void onMessage(Message msg) throws IOException, MessagingException {
        boolean withTextContent = true;
        boolean withHtmlContent = false;
        boolean preferHtmlContent = false;
        boolean withAttachments = false;
        boolean stripTagsFromTextContent = true;
        List<String> headersToFields = ImmutableList.of();
        final IndexableMailMessage imsg = IndexableMailMessage.fromJavaMailMessage(msg, withTextContent, withHtmlContent, preferHtmlContent, withAttachments,
                stripTagsFromTextContent, headersToFields);
        logger.info(mapper.writeValueAsString(imsg));

    }

    @Override
    public void onMessageDeletes(Set msgs, String folderName, boolean isPop) throws IOException, MessagingException {

    }

    @Override
    public ElasticsearchMailDestination startup() throws IOException {
        return null;
    }
}
