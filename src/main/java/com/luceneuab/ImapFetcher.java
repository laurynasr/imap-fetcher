package com.luceneuab;

import de.saly.elasticsearch.mailsource.ParallelPollingIMAPMailSource;
import de.saly.elasticsearch.riverstate.RiverState;
import de.saly.elasticsearch.riverstate.RiverStateManager;
import org.elasticsearch.common.logging.ESLogger;
import org.elasticsearch.common.logging.ESLoggerFactory;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Properties;

public class ImapFetcher {

    protected final ESLogger logger = ESLoggerFactory.getLogger(this.getClass().getName());

    public void fetchAll(String server, String user, String passwd) throws MessagingException, IOException {
        final Properties props = new Properties();

        props.put("mail.store.protocol", "imap");

        props.put("mail.imap.host", server);
        props.put("mail.imap.port", 993);
        props.put("mail.imap.ssl.enable", true);
        props.put("mail.imap.connectionpoolsize", "1");
        props.put("mail.debug", true);
        props.put("mail.imap.timeout", 10000);
        ParallelPollingIMAPMailSource source = new ParallelPollingIMAPMailSource(props, 1, user, passwd);
        source.setMailDestination(new LoggableMailDestination());
        source.setStateManager(new MockRiverStateManager());
        source.fetchAll();
    }

    private static class MockRiverStateManager implements RiverStateManager {
        @Override
        public RiverState getRiverState(Folder folder) throws MessagingException {
            return new RiverState();
        }

        @Override
        public void onError(String errmsg, Folder folder, Exception e) {

        }

        @Override
        public void onError(String errmsg, Message msg, Exception e) {

        }

        @Override
        public void setRiverState(RiverState state) throws MessagingException {

        }
    }
}
